------------------------------------- SETUP
-- Make sure you have permission to create a database and stored procedures.
/*

-- CREATE THE Backup Database with a simple recovery model. Please note 'Backup' will need to be wrapped in brackets.
CREATE DATABASE [Backup];
ALTER DATABASE [Backup] SET RECOVERY SIMPLE;

-- Create a schema on the backup database with the name of the database you will be deploying the stored proedure to.
USE [Backup];
CREATE SCHEMA  [YourDatabaseName];

*/
----------------------------------------------------------------- BEGIN sp_BackupTable Procedure Creation
USE [YOUR DATABASE HERE]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[sp_BackupTable] (@tbl VARCHAR(256)) AS
----  Example: EXEC sp_BackupTable 'tablenamehere'

----  Declare a string value with a length of 2048. This will be used to hold the sql query generated.
DECLARE @sql VARCHAR(2048);

---- Declare a suffix to the table name. This is a timestamp down to the second. i.e. 2018109101148 Year+Month+Day+Hour+Minute+Second. 
---- This is to reassure if multiple users are backing the same table, there will be little chance for conflict with table naming
DECLARE @end VARCHAR(14) = CAST(DATEPART(YEAR,GETDATE()) AS VARCHAR(4))
							+ RIGHT('00'+CAST(DATEPART(MONTH,GETDATE()) AS VARCHAR(2)),2) 
							+ CAST(DATEPART(DAY,GETDATE()) AS VARCHAR(2))
							+ CAST(DATEPART(HOUR,GETDATE()) AS VARCHAR(2)) 
							+ RIGHT('00'+CAST(DATEPART(MINUTE,GETDATE()) AS VARCHAR(2)),2)
							+ RIGHT('00'+CAST(DATEPART(SECOND,GETDATE()) AS VARCHAR(2)),2);

---- set the @sql variable with the query
SET @sql = 'SELECT * INTO [BackupDB].[YourDatabaseName].'+@tbl+'_'+@end+' FROM '+@tbl;

---- Check to see if the table you are backing up exists in the database.
IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES 
           WHERE TABLE_NAME = @tbl)
---- If exists, run the generated @sql
BEGIN
	EXEC (@sql)
---- Display generated table name when the query is complete. 
	PRINT 'Table Name: [BackupDB].chcTempo.'+@tbl+'_'+@end
END
---- If source table is not found, then notify user.
	ELSE
	PRINT 'Table Not Found'

GO


